call plug#begin('~/.vim/plugged')
if !exists('g:vscode') 
  syntax enable   " enable syntax processing
  set tabstop=2   " number of visual spaces per tab
  set shiftwidth=2
  set softtabstop=2   " number of spaces in tab when editing
  set expandtab   " tabs are space
  set smarttab
  set ignorecase
  set smartcase   " case insensitive search by default, but switches to case sensitive if you use capital letters
  set number      " show linenumbers
  set showcmd     " show last command in bottom bar
  set wildmenu    " visual autocomplete for command menu
  set showmatch   " highlight matching braces
  set hlsearch    " highlight search results
  set linebreak       " wraps on words instead of characters

  filetype plugin indent on

  set hidden " not really sure, i believe it makes it so you can switch buffers without saving
  set mouse=a " fuck it, I wanna resize windows using my mouse. It's just a pain in the butt to do it any other way

  " little script for toggling hybrid line number mode in normal mode,
  " but not in insert mode or when the buffer doesn't have focus
  set number
  augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu   | endif
    autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu | endif
  augroup END

  let g:airline#extensions#tabline#enabled = 1

  let g:airline_powerline_fonts = 1 " Power lines font

  " Auto install vim-plug, as in https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation
  let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
  if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif

  call plug#begin('~/.vim/plugged')

  Plug 'davidhalter/jedi-vim'
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  Plug 'zchee/deoplete-jedi'
  Plug 'vim-airline/vim-airline'
  "        Plug 'vim-airline/vim-airline-themes'
  Plug 'sbdchd/neoformat'
  Plug 'neomake/neomake'
  Plug 'lervag/vimtex'
  Plug 'arcticicestudio/nord-vim'
  Plug 'preservim/nerdtree'
  Plug 'junegunn/fzf'
  Plug 'junegunn/fzf.vim'
  Plug 'airblade/vim-gitgutter'
  Plug 'gpanders/vim-oldfiles'

  call plug#end()

  colorscheme nord

  let g:deoplete#enable_at_startup = 1

  "        let g:airline_theme='solarized'
  "        let g:airline_solarized_bg='light'

  let g:neomake_python_enabled_makers = ['pylint']
  call neomake#configure#automake('nrwi', 500)

  " NERDTree shortcut
  nnoremap <C-b> :NERDTreeToggleVCS<CR>
  " nnoremap <C-n> :NERDTreeToggle<CR>

  " Persistent Undo
  set dir=~/.vim/swapfiles
  set backup
  set backupdir=~/.vim/backupfiles
  set undofile
  set undodir=~/.vim/undofiles

  " Custom functions

  " save as sudo
  cnoremap w!! execute 'silent! write !SUDO_ASKPASS=`which ssh-askpass` sudo tee % >/dev/null' <bar> edit!

  " Fzf custom functions
  command! -bang HomeFiles call fzf#vim#files('~/', <bang>0)
  command! -bang AllFiles call fzf#vim#files('~/', <bang>0)
  " Keybindings
  "  map - <Nop>

  " Leader key and 1 to reload vimrc
  nnoremap <leader>1 :source ~/.vimrc<CR>

  nnoremap <C-p> :Files<CR>
  nnoremap <leader>p :HomeFiles<CR>
  nnoremap <C-h> :History<CR>
  nnoremap <C-g> :Rg<CR>
  nnoremap <A-Tab> :Buffers<CR>
  nnoremap <Tab> :bnext<CR>
  nnoremap <S-Tab> :bprevious<CR>
  nnoremap <leader>c :Commands<CR>
  nnoremap <leader>w :bp<bar>sp<bar>bn<bar>bd<CR>
  nnoremap <leader>e :NERDTreeFocus<CR>
  nnoremap <C-s> :w<CR>
  nnoremap <C-q> :q<CR>

  nnoremap <S-C-I> :Neoformat<CR>

  " format on save
  augroup fmt
    autocmd!
    autocmd BufWritePre * undojoin | Neoformat
  augroup END

endif

" beyond this, everything also applies to vscode nvim

Plug 'bkad/CamelCaseMotion'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'easymotion/vim-easymotion'

call plug#end()

let mapleader = "-"
map <silent> w <Plug>CamelCaseMotion_w
map <silent> b <Plug>CamelCaseMotion_b
map <silent> e <Plug>CamelCaseMotion_e
map <silent> ge <Plug>CamelCaseMotion_ge
sunmap w
sunmap b
sunmap e
sunmap ge
omap <silent> iw <Plug>CamelCaseMotion_iw
xmap <silent> iw <Plug>CamelCaseMotion_iw
omap <silent> ib <Plug>CamelCaseMotion_ib
xmap <silent> ib <Plug>CamelCaseMotion_ib
omap <silent> ie <Plug>CamelCaseMotion_ie
xmap <silent> ie <Plug>CamelCaseMotion_ie
imap <silent> <S-Left> <C-o><Plug>CamelCaseMotion_b
imap <silent> <S-Right> <C-o><Plug>CamelCaseMotion_w

" Use system clipboard as default register
set clipboard=unnamedplus
