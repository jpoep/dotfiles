#! /bin/sh
cd "$(dirname "${BASH_SOURCE[0]}")"

source ./variables.sh;

sleep_pid=0

toggle() {

    if [ "$sleep_pid" -ne 0 ]; then
        ./toggle-sink.sh
        kill $sleep_pid >/dev/null 2>&1
    fi
}

trap "toggle" USR1

while true; do
        if [ $(pactl get-default-sink) = $HEADPHONES_SINK ]; then
                echo ""
        elif [ $(pactl get-default-sink) = $SPEAKERS_SINK ]; then
                echo "蓼"
        else 
                echo "?"
        fi
        sleep 3600 &
        sleep_pid=$!
        wait
done
