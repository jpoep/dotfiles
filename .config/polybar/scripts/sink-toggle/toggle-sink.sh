#! /bin/sh
cd "$(dirname "${BASH_SOURCE[0]}")"

source ./variables.sh

if [ $(pactl get-default-sink) = $HEADPHONES_SINK ]; then
        pactl set-default-sink $SPEAKERS_SINK
elif [ $(pactl get-default-sink) = $SPEAKERS_SINK ]; then
        pactl set-default-sink $HEADPHONES_SINK
else 
        pactl set-default-sink $HEADPHONES_SINK
fi

exit 1
