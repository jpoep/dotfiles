#!/bin/bash
TMPFILE="$(mktemp -t screencast-XXXXXXX).mp4"
OUTPUT="$HOME/data/screencasts/$(date +%F-%H-%M-%S)"

read -r X Y W H G ID < <(slop -f "%x %y %w %h %g %i")
ffmpeg -f x11grab -s "$W"x"$H" -i :0.0+$X,$Y "$TMPFILE"

mv $TMPFILE $OUTPUT.mp4

xdg-open $OUTPUT.mp4

trap "rm -f '$TMPFILE'" 0
