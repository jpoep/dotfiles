# !bin/sh
if [ -z $DOTFILES_PLATFORM ]; then
        echo "How is this device called?"
        read device_id
else
        device_id=$DOTFILES_PLATFORM
fi

# .zshrc

if [ ! -f $HOME/.zshrc.$device_id ]; then
    touch $HOME/.zshrc.$device_id
    echo "Created $HOME/.zshrc.$device_id"
else
    echo "$HOME/.zshrc.$device_id already exists."
fi

if [ ! -f $HOME/.zshrc.local ]; then
    ln -s $HOME/.zshrc.$device_id $HOME/.zshrc.local
    echo "Linked $HOME/.zshrc.$device_id to $HOME/.zshrc.local"
else
    echo "$HOME/.zshrc.local already exists."
fi

# .zshenv

if [ ! -f $HOME/.zshenv.$device_id ]; then
    touch $HOME/.zshenv.$device_id
    echo "export DOTFILES_DEVICE=$device_id" >> $HOME/.zshenv.$device_id
    echo "Created $HOME/.zshenv.$device_id"
else
    echo "$HOME/.zshenv.$device_id already exists."
fi

if [ ! -f $HOME/.zshenv.local ]; then
    ln -s $HOME/.zshenv.$device_id $HOME/.zshenv.local
    echo "Linked $HOME/.zshenv.$device_id to $HOME/.zshenv.local"
else
    echo "$HOME/.zshenv.local already exists."
fi

# .gitconfig

if [ ! -f $HOME/.gitconfig.$device_id ]; then
    touch $HOME/.gitconfig.$device_id
    echo "Created $HOME/.gitconfig.$device_id"
else
    echo "$HOME/.gitconfig.$device_id already exists."
fi

if [ ! -f $HOME/.gitconfig.local ]; then
    ln -s $HOME/.gitconfig.$device_id $HOME/.gitconfig.local
    echo "Linked $HOME/.gitconfig.$device_id to $HOME/.gitconfig.local"
else
    echo "$HOME/.gitconfig.local already exists."
fi

# Pacman hook

echo "Symlinking pacman hook to hook directory..."
PACMAN_HOOK_DIR=/usr/share/libalpm/hooks 
if [ ! -f $PACMAN_HOOK_DIR/explicit-install-list.hook ]; then
    sudo ln -s $INSTALL_HOOK $PACMAN_HOOK_DIR/explicit-install-list.hook
    echo "Linked $INSTALL_HOOK to $PACMAN_HOOK_DIR"
else
    "But the hook already exists there."
fi

SCRIPTS_DIR=/usr/local/bin
echo "Linking scripts to $SCRIPTS_DIR..."
sudo ln -s $HOME/.config/scripts/* $SCRIPTS_DIR

echo "Generating package list for the first time..."
sudo pacman -Qqe > $HOME/.pkglist.$device_id.txt

echo "Import Gnome keybindings? All of the stuff you have already defined will be overwritten! (y/N)"
read keybinding_response

if [ "$keybinding_response" = "y" ]; then
	sync-keybindings.sh import
fi

echo "Add and commit .pkglist.$device_id.txt and .zshrc.$device_id? (y/N)"
read commit_response

if [ "$commit_response" = "y" ]; then
    dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"
    $dotfiles add $HOME/.pkglist.$device_id.txt
    $dotfiles add $HOME/.zshrc.$device_id
    $dotfiles commit -m "Add device configuration for $device_id"
fi

