#!/bin/zsh
# any_term_dropdown.sh - turns any terminal into a dropdown terminal
# tutorial video: https://www.youtube.com/watch?v=mVw2gD9iiOg
#             _   _     _      _         
#  __ _  ___ | |_| |__ | | ___| |_ _   _ 
# / _` |/ _ \| __| '_ \| |/ _ \ __| | | |
#| (_| | (_) | |_| |_) | |  __/ |_| |_| |
# \__, |\___/ \__|_.__/|_|\___|\__|\__,_|
# |___/                                  
#       http://www.youtube.com/user/gotbletu
#       https://twitter.com/gotbletu
#       https://www.facebook.com/gotbletu
#       https://plus.google.com/+gotbletu
#       https://github.com/gotbletu

# requires:
# 	xdotool
# 	wmutils https://github.com/wmutils/core
#		https://aur.archlinux.org/packages/wmutils-git/ 

# get screen resolution width and height
ROOT=$(lsw -r)
width=$(wattr w $ROOT)
height=$(wattr h $ROOT)

my_term=kitty

# get terminal emulator pid ex: 44040485
pid=$(xdotool search --class "$my_term" | tail -n1)

if [[ $pid ]]
then 
    # get windows id from pid ex: 0x2a00125%
    wid=$(printf 0x%x "$pid")

    # toggle show/hide terminal emulator
    mapw -t "$wid"	
else
    (kitty &)
fi
