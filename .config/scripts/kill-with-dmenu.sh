#!/usr/bin/env bash

wofi="wofi -d --style=/home/jonas/.config/wofi/style-monospace.css"
echo $wofi

selected="$(ps -eo pid,cmd --forest | \
            $wofi -i -p "Type to search and select process to kill" \
            awk '{print $1" "$4}')"; 

if [[ ! -z $selected ]]; then

    answer="$(echo -e "Yes\nNo" | \
            $wofi -i -p "$selected will be killed, are you sure?" \
            $lines $colors $font )"

    if [[ $answer == "Yes" ]]; then
        selpid="$(awk '{print $1}' <<< $selected)"; 
        kill -9 $selpid
    fi
fi

exit 0
