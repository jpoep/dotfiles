#!/bin/zsh

gnome_settings_path="/org/gnome/settings-daemon/plugins/media-keys/"
current_keybindings=`dconf dump $gnome_settings_path` 
keybindings_file=$HOME/.gnome-keybindings

if [ "$1" = "export" ]; then
	echo "Exporting your config to $keybindings_file"
	echo $current_keybindings > $keybindings_file
elif [ "$1" = "import" ]; then
	echo "Importing your config from $keybindings_file"
	if [ -f "$keybindings_file" ]; then
		dconf_output=`dconf load -f $gnome_settings_path < $keybindings_file`
		echo "$dconf_output"
		echo "If the only message you see is this one, it was probably successful."
	else
		echo "But the file doesn't exist, so nothing happened."
	fi
elif [ -f "$keybindings_file" ]; then
	diff_result=`diff -u --color <(echo "$current_keybindings") $keybindings_file`
	if [ -z $diff_result ]; then
		echo "Settings are identical. No action needed."
	else
		echo "Here's the diff for your keybindings: "
		echo $diff_result
		echo "Please run the script with the import command for importing the file contents to your local system using the export command for dumping your current configuration to the file. Note that no merging is performed whatsoever - you simply either override your local config or the remote config."
	fi
else
	echo "No file named $keybindings_file was found. Creating one using your config..."
	echo $current_keybindings > $keybindings_file
fi
