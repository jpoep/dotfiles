#!/bin/zsh

if [ -z "$1" ]
then
    echo "No alias name supplied"
	  exit
fi

if [ -n "$2" ]
then 
    ALIAS_COMMAND=$2
else
				ALIAS_COMMAND=$(tail -n2 $HOME/.zsh_history | head -n1 | cut -d';' -f2)
fi

echo "alias $1='$ALIAS_COMMAND'" >> $HOME/.aliases.sh
echo "Created alias $1 for command $ALIAS_COMMAND"
