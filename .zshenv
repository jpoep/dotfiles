export FZF_DEFAULT_OPTS='--height 90% --layout=reverse --border --inline-info'
export FZF_DEFAULT_COMMAND='fd --hidden --type f'
export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND
export FZF_CTRL_T_OPTS='--preview "bat --color always {}"'
export FZF_ALT_C_COMMAND='fd --hidden --type d'
export FZF_ALT_C_OPTS='--preview "tree -C {} | head -100"'

export _fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}

# Use fd to generate the list for directory completion
export _fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1" 
}

export YTFZF_ENABLE_FZF_DEFAULT_OPTS=1

source $HOME/.zshenv.local
