# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.

sleep 0.1 # workaround to stop percent sign from appearing when starting the terminal, see https://github.com/lxqt/qterminal/issues/778#issuecomment-908931984
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

ZSH=/usr/share/oh-my-zsh/

# enable for profiling; also remember to scroll to the end of the file and uncomment there
# zmodload zsh/zprof

ZSH_THEME="powerlevel10k/powerlevel10k"
DEFAULT_USER=jonas

HYPHEN_INSENSITIVE="true"

COMPLETION_WAITING_DOTS="true"

HIST_STAMPS="yyyy-mm-dd"
SAVEHIST=999999999

plugins=(git capistrano bundler rake rvm autojump python pip gnu-utils history-substring-search zsh-autosuggestions zsh-syntax-highlighting) 

ZSH_AUTOSUGGEST_STRATEGY=(completion history)
ZSH_AUTOSUGGEST_USE_ASYNC="true"
bindkey "^[e" autosuggest-accept

# User configuration

# Nvim everywhere
export EDITOR='/usr/bin/nvim'
export GIT_EDITOR='/usr/bin/nvim'
export VISUAL='/usr/bin/nvim'
export DIFFPROG='nvim -d'
export MANPAGER='nvim +Man!'
export MANWIDTH=999

# ssh
export SSH_KEY_PATH="~/.ssh/rsa_id"

ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
if [[ ! -d $ZSH_CACHE_DIR ]]; then
  mkdir $ZSH_CACHE_DIR
fi

source $ZSH/oh-my-zsh.sh
source /usr/share/nvm/init-nvm.sh
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
source /usr/share/fzf-tab-completion/zsh/fzf-zsh-completion.sh

# Aliases

source $HOME/.aliases.sh

export PATH="$PATH:$(yarn global bin):$HOME/.local/bin"
export PATH="$PATH:$HOME/.config/scripts"
export PATH="$PATH:$HOME/.cargo/bin"
export PLANTUML_JAR=/usr/share/java/plantuml/plantuml.jar
export BROWSER=/usr/bin/firefox

# Function for combining mkdir and cd
mkcdir ()
{
        mkdir -p -- "$1" &&
        cd -P -- "$1"
}

rpg () {
    rpg-cli "$@"
    cd "$(rpg-cli pwd)"
}

# Zoxide
#
eval "$(zoxide init zsh)"

source $HOME/.zshrc.local

# eval $(thefuck --alias)

# uncomment for profiling
# zprof

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
