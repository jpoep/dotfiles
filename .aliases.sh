alias wine='PULSE_LATENCY_MSEC=60 wine'
alias kapeel=yarn
alias joms="wol 50:E5:49:D1:88:F5"
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"
alias dof=dotfiles
alias dis="xdg-open ."
alias vim=nvim
alias owf=aiksaurus
alias simcc="cd ~/projects/simc/AutoSimC"
alias packages="pacman -Slq | fzf --multi --preview 'pacman -Si {1}' | xargs -ro sudo pacman -S"
alias ls="exa --icons"
alias ranger="source ranger"

alias zrc="$VISUAL ~/.zshrc"
alias zrcd="$VISUAL ~/.zshrc.desktop"
alias zrci="$VISUAL ~/.zshrc.i3-desktop"
alias zrcl="$VISUAL ~/.zshrc.laptop"

alias paper="code ~/projects/papers/research-project"
alias fyf="fzf"
alias dofc='dof commit'
alias dofp='dof push'
alias mt='~/projects/master-thesis'
alias dofl='dof pull'
alias dofa='dof add'
alias dofs='dof status'
alias refresh='source ~/.zshrc'
alias mt="~/projects/papers/master-thesis"
alias ssh="kitty +kitten ssh"
alias vimh='vim -c "History"'
alias kekw="echo kekw"
alias p='cd ~/projects'
alias y='z'
alias yi='zi'
alias se='sudoedit'
alias dow='cd ~/Downloads'
alias yeet='kill -9'
