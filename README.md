# dotfiles
My dotfiles. See <https://harfangk.github.io/2016/09/18/manage-dotfiles-with-a-git-bare-repository.html>.

Do the following first:
```
echo 'alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"' >> $HOME/.zshrc
source ~/.zshrc
echo ".dotfiles.git" >> .gitignore
git clone --bare git@gitlab.com:jpoep/dotfiles.git $HOME/.dotfiles.git
dotfiles checkout
dotfiles config --local status.showUntrackedFiles no
```
After checkout, make sure to execute `.config/scripts/init_dotfiles.sh`.
